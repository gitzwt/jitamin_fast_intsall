/*
 Navicat Premium Data Transfer

 Source Server         : wamp
 Source Server Type    : MySQL
 Source Server Version : 50720
 Source Host           : localhost:3306
 Source Schema         : jitamin

 Target Server Type    : MySQL
 Target Server Version : 50720
 File Encoding         : 65001

 Date: 28/09/2018 12:17:31
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for action_has_params
-- ----------------------------
DROP TABLE IF EXISTS `action_has_params`;
CREATE TABLE `action_has_params`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `action_id`(`action_id`) USING BTREE,
  CONSTRAINT `action_has_params_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for actions
-- ----------------------------
DROP TABLE IF EXISTS `actions`;
CREATE TABLE `actions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `event_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `action_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  CONSTRAINT `actions_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for column_has_move_restrictions
-- ----------------------------
DROP TABLE IF EXISTS `column_has_move_restrictions`;
CREATE TABLE `column_has_move_restrictions`  (
  `restriction_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `src_column_id` int(11) NOT NULL,
  `dst_column_id` int(11) NOT NULL,
  PRIMARY KEY (`restriction_id`) USING BTREE,
  UNIQUE INDEX `role_id`(`role_id`, `src_column_id`, `dst_column_id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  INDEX `src_column_id`(`src_column_id`) USING BTREE,
  INDEX `dst_column_id`(`dst_column_id`) USING BTREE,
  CONSTRAINT `column_has_move_restrictions_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `column_has_move_restrictions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `project_has_roles` (`role_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `column_has_move_restrictions_ibfk_3` FOREIGN KEY (`src_column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `column_has_move_restrictions_ibfk_4` FOREIGN KEY (`dst_column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for column_has_restrictions
-- ----------------------------
DROP TABLE IF EXISTS `column_has_restrictions`;
CREATE TABLE `column_has_restrictions`  (
  `restriction_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `rule` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`restriction_id`) USING BTREE,
  UNIQUE INDEX `role_id`(`role_id`, `column_id`, `rule`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  INDEX `column_id`(`column_id`) USING BTREE,
  CONSTRAINT `column_has_restrictions_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `column_has_restrictions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `project_has_roles` (`role_id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `column_has_restrictions_ibfk_3` FOREIGN KEY (`column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for columns
-- ----------------------------
DROP TABLE IF EXISTS `columns`;
CREATE TABLE `columns`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `position` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `task_limit` int(11) NULL DEFAULT 0,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `hide_in_dashboard` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `title`(`title`, `project_id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  CONSTRAINT `columns_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for comments
-- ----------------------------
DROP TABLE IF EXISTS `comments`;
CREATE TABLE `comments`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT 0,
  `date_creation` bigint(20) NULL DEFAULT NULL,
  `comment` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `reference` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `reference`(`reference`) USING BTREE,
  INDEX `task_id`(`task_id`) USING BTREE,
  CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for custom_filters
-- ----------------------------
DROP TABLE IF EXISTS `custom_filters`;
CREATE TABLE `custom_filters`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `filter` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_shared` tinyint(1) NULL DEFAULT 0,
  `append` tinyint(1) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  CONSTRAINT `custom_filters_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `custom_filters_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for group_has_users
-- ----------------------------
DROP TABLE IF EXISTS `group_has_users`;
CREATE TABLE `group_has_users`  (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  UNIQUE INDEX `group_id`(`group_id`, `user_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `group_has_users_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `group_has_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `external_id` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for last_logins
-- ----------------------------
DROP TABLE IF EXISTS `last_logins`;
CREATE TABLE `last_logins`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `auth_type` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `ip` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `date_creation` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `last_logins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of last_logins
-- ----------------------------
INSERT INTO `last_logins` VALUES (1, 'Database', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1538107837);

-- ----------------------------
-- Table structure for links
-- ----------------------------
DROP TABLE IF EXISTS `links`;
CREATE TABLE `links`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `opposite_id` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `label`(`label`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of links
-- ----------------------------
INSERT INTO `links` VALUES (1, 'relates to', 0);
INSERT INTO `links` VALUES (2, 'blocks', 3);
INSERT INTO `links` VALUES (3, 'is blocked by', 2);
INSERT INTO `links` VALUES (4, 'duplicates', 5);
INSERT INTO `links` VALUES (5, 'is duplicated by', 4);
INSERT INTO `links` VALUES (6, 'is a child of', 7);
INSERT INTO `links` VALUES (7, 'is a parent of', 6);
INSERT INTO `links` VALUES (8, 'targets milestone', 9);
INSERT INTO `links` VALUES (9, 'is a milestone of', 8);
INSERT INTO `links` VALUES (10, 'fixes', 11);
INSERT INTO `links` VALUES (11, 'is fixed by', 10);

-- ----------------------------
-- Table structure for migrations
-- ----------------------------
DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations`  (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `start_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_time` timestamp(0) NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `breakpoint` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`version`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of migrations
-- ----------------------------
INSERT INTO `migrations` VALUES (20161222061456, 'CreateUsersTable', '2018-09-28 04:09:34', '2018-09-28 04:09:34', 0);
INSERT INTO `migrations` VALUES (20161222065743, 'CreateRememberMeTable', '2018-09-28 04:09:34', '2018-09-28 04:09:34', 0);
INSERT INTO `migrations` VALUES (20161222071058, 'CreateGroupsTable', '2018-09-28 04:09:34', '2018-09-28 04:09:34', 0);
INSERT INTO `migrations` VALUES (20161222071513, 'CreateSettingsTable', '2018-09-28 04:09:34', '2018-09-28 04:09:34', 0);
INSERT INTO `migrations` VALUES (20161222072332, 'CreateProjectsTable', '2018-09-28 04:09:34', '2018-09-28 04:09:34', 0);
INSERT INTO `migrations` VALUES (20161222073541, 'CreateActionsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222073852, 'CreateColumnsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222074452, 'CreateTasksTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222081719, 'CreateCommentsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222082417, 'CreateSwimlanesTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222083010, 'CreateTagsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222083245, 'CreateSubtasksTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222083935, 'CreateLinksTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222084249, 'CreateTransitionsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222084940, 'CreateCustomFiltersTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222085354, 'CreateLastLoginsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222085809, 'CreatePasswordResetTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222091052, 'CreatePluginSchemaVersionsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222091605, 'CreateProjectActivitiesTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222092217, 'CreateProjectDailyColumnStatsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222092312, 'CreateProjectDailyStatsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222093033, 'CreateSchemaVersionTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222093333, 'CreateActionHasParamsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222094356, 'CreateProjectHasRolesTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222094851, 'CreateColumnHasRestrictionsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222094859, 'CreateColumnHasMoveRestrictionsTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222095207, 'CreateGroupHasUsersTable', '2018-09-28 04:09:35', '2018-09-28 04:09:35', 0);
INSERT INTO `migrations` VALUES (20161222095739, 'CreateProjectHasCategoriesTable', '2018-09-28 04:09:35', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222100221, 'CreateProjectHasFilesTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222104316, 'CreateProjectHasGroupsTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222104338, 'CreateProjectHasMetadataTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222104355, 'CreateProjectHasStarsTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222104411, 'CreateProjectHasNotificationTypesTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222104427, 'CreateProjectHasUsersTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222112306, 'CreateProjectRoleHasRestrictionsTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222112615, 'CreateSubtaskTimeTrackingTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222113157, 'CreateTaskHasExternalLinksTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222113205, 'CreateTaskHasFilesTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222113217, 'CreateTaskHasLinksTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222113234, 'CreateTaskHasMetadataTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222113239, 'CreateTaskHasTagsTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222114814, 'CreateUserHasMetadataTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222114828, 'CreateUserHasNotificationTypesTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222114837, 'CreateUserHasNotificationsTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161222114844, 'CreateUserHasUnreadNotificationsTable', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161225123941, 'AlterTableUsersAddApiTokenColumn', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161228031419, 'AlterTableUsersAddLayoutColumn', '2018-09-28 04:09:36', '2018-09-28 04:09:36', 0);
INSERT INTO `migrations` VALUES (20161231134810, 'AlterTableUsersAddDashboardColumn', '2018-09-28 04:09:36', '2018-09-28 04:09:37', 0);
INSERT INTO `migrations` VALUES (20170105040003, 'AlterTableProjectsAddDefaultViewColumn', '2018-09-28 04:09:37', '2018-09-28 04:09:37', 0);
INSERT INTO `migrations` VALUES (20171228053201, 'AlterTableActionsAddPositionColumn', '2018-09-28 04:09:37', '2018-09-28 04:09:37', 0);

-- ----------------------------
-- Table structure for password_reset
-- ----------------------------
DROP TABLE IF EXISTS `password_reset`;
CREATE TABLE `password_reset`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_expiration` int(11) NOT NULL,
  `date_creation` int(11) NOT NULL,
  `ip` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `password_reset_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for plugin_schema_versions
-- ----------------------------
DROP TABLE IF EXISTS `plugin_schema_versions`;
CREATE TABLE `plugin_schema_versions`  (
  `plugin` varchar(80) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `version` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`plugin`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_activities
-- ----------------------------
DROP TABLE IF EXISTS `project_activities`;
CREATE TABLE `project_activities`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `date_creation` bigint(20) NULL DEFAULT NULL,
  `event_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `creator_id` int(11) NULL DEFAULT NULL,
  `project_id` int(11) NULL DEFAULT NULL,
  `task_id` int(11) NULL DEFAULT NULL,
  `data` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `creator_id`(`creator_id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  INDEX `task_id`(`task_id`) USING BTREE,
  CONSTRAINT `project_activities_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `project_activities_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `project_activities_ibfk_3` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_daily_column_stats
-- ----------------------------
DROP TABLE IF EXISTS `project_daily_column_stats`;
CREATE TABLE `project_daily_column_stats`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `total` int(11) NOT NULL DEFAULT 0,
  `score` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `day`(`day`, `project_id`, `column_id`) USING BTREE,
  INDEX `column_id`(`column_id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  CONSTRAINT `project_daily_column_stats_ibfk_1` FOREIGN KEY (`column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `project_daily_column_stats_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_daily_stats
-- ----------------------------
DROP TABLE IF EXISTS `project_daily_stats`;
CREATE TABLE `project_daily_stats`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `day` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `avg_lead_time` int(11) NOT NULL DEFAULT 0,
  `avg_cycle_time` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `day`(`day`, `project_id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  CONSTRAINT `project_daily_stats_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_has_categories
-- ----------------------------
DROP TABLE IF EXISTS `project_has_categories`;
CREATE TABLE `project_has_categories`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `position` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `project_id`(`project_id`, `name`) USING BTREE,
  CONSTRAINT `project_has_categories_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_has_files
-- ----------------------------
DROP TABLE IF EXISTS `project_has_files`;
CREATE TABLE `project_has_files`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_image` tinyint(1) NULL DEFAULT 0,
  `size` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `date` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  CONSTRAINT `project_has_files_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_has_groups
-- ----------------------------
DROP TABLE IF EXISTS `project_has_groups`;
CREATE TABLE `project_has_groups`  (
  `group_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `group_id`(`group_id`, `project_id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  CONSTRAINT `project_has_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `project_has_groups_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_has_metadata
-- ----------------------------
DROP TABLE IF EXISTS `project_has_metadata`;
CREATE TABLE `project_has_metadata`  (
  `project_id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `changed_by` int(11) NOT NULL DEFAULT 0,
  `changed_on` int(11) NOT NULL DEFAULT 0,
  UNIQUE INDEX `project_id`(`project_id`, `name`) USING BTREE,
  CONSTRAINT `project_has_metadata_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_has_notification_types
-- ----------------------------
DROP TABLE IF EXISTS `project_has_notification_types`;
CREATE TABLE `project_has_notification_types`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `notification_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `project_id`(`project_id`, `notification_type`) USING BTREE,
  CONSTRAINT `project_has_notification_types_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_has_roles
-- ----------------------------
DROP TABLE IF EXISTS `project_has_roles`;
CREATE TABLE `project_has_roles`  (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`role_id`) USING BTREE,
  UNIQUE INDEX `project_id`(`project_id`, `role`) USING BTREE,
  CONSTRAINT `project_has_roles_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_has_stars
-- ----------------------------
DROP TABLE IF EXISTS `project_has_stars`;
CREATE TABLE `project_has_stars`  (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  UNIQUE INDEX `project_id`(`project_id`, `user_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `project_has_stars_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `project_has_stars_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_has_users
-- ----------------------------
DROP TABLE IF EXISTS `project_has_users`;
CREATE TABLE `project_has_users`  (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  UNIQUE INDEX `project_id`(`project_id`, `user_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `project_has_users_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `project_has_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for project_role_has_restrictions
-- ----------------------------
DROP TABLE IF EXISTS `project_role_has_restrictions`;
CREATE TABLE `project_role_has_restrictions`  (
  `restriction_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `rule` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`restriction_id`) USING BTREE,
  UNIQUE INDEX `role_id`(`role_id`, `rule`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  CONSTRAINT `project_role_has_restrictions_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `project_role_has_restrictions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `project_has_roles` (`role_id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS `projects`;
CREATE TABLE `projects`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_active` tinyint(1) NULL DEFAULT 1,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `last_modified` bigint(20) NULL DEFAULT NULL,
  `is_public` tinyint(1) NULL DEFAULT 0,
  `is_private` tinyint(1) NULL DEFAULT 0,
  `is_everybody_allowed` tinyint(1) NULL DEFAULT 0,
  `default_swimlane` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT 'Default swimlane',
  `default_view` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `show_default_swimlane` int(11) NULL DEFAULT 1,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `identifier` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `start_date` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `end_date` varchar(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `owner_id` int(11) NULL DEFAULT 0,
  `priority_default` int(11) NULL DEFAULT 0,
  `priority_start` int(11) NULL DEFAULT 0,
  `priority_end` int(11) NULL DEFAULT 3,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for remember_me
-- ----------------------------
DROP TABLE IF EXISTS `remember_me`;
CREATE TABLE `remember_me`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NULL DEFAULT NULL,
  `ip` varchar(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `user_agent` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `sequence` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `expiration` int(11) NULL DEFAULT NULL,
  `date_creation` bigint(20) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `remember_me_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of remember_me
-- ----------------------------
INSERT INTO `remember_me` VALUES (1, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '31c9421840a66d736da015184afd509875ea97630eb55ed14b9db2dd85859049', '49a8442ae27c6ced43b329ae6a36d0ede4753fcf360d6fa21d1cef4e414d', 1540699837, 1538107837);

-- ----------------------------
-- Table structure for schema_version
-- ----------------------------
DROP TABLE IF EXISTS `schema_version`;
CREATE TABLE `schema_version`  (
  `version` int(11) NULL DEFAULT 0
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for settings
-- ----------------------------
DROP TABLE IF EXISTS `settings`;
CREATE TABLE `settings`  (
  `option` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `changed_by` int(11) NOT NULL DEFAULT 0,
  `changed_on` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`option`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of settings
-- ----------------------------
INSERT INTO `settings` VALUES ('api_token', 'd8ea8087f454daa78d4dde7758f83f99cf7196591e28e927f6eacecdafbd', 0, 0);
INSERT INTO `settings` VALUES ('application_datetime_format', 'm/d/Y H:i', 1, 1538107872);
INSERT INTO `settings` VALUES ('application_date_format', 'm/d/Y', 1, 1538107872);
INSERT INTO `settings` VALUES ('application_language', 'zh_CN', 1, 1538107872);
INSERT INTO `settings` VALUES ('application_skin', 'default', 0, 0);
INSERT INTO `settings` VALUES ('application_timezone', 'UTC', 1, 1538107872);
INSERT INTO `settings` VALUES ('application_time_format', 'H:i', 1, 1538107872);
INSERT INTO `settings` VALUES ('board_columns', '', 0, 0);
INSERT INTO `settings` VALUES ('board_highlight_period', '172800', 0, 0);
INSERT INTO `settings` VALUES ('board_private_refresh_interval', '10', 0, 0);
INSERT INTO `settings` VALUES ('board_public_refresh_interval', '60', 0, 0);
INSERT INTO `settings` VALUES ('calendar_project_tasks', 'date_started', 0, 0);
INSERT INTO `settings` VALUES ('calendar_user_subtasks_time_tracking', '0', 0, 0);
INSERT INTO `settings` VALUES ('calendar_user_tasks', 'date_started', 0, 0);
INSERT INTO `settings` VALUES ('cfd_include_closed_tasks', '1', 0, 0);
INSERT INTO `settings` VALUES ('default_color', 'yellow', 0, 0);
INSERT INTO `settings` VALUES ('integration_gravatar', '0', 0, 0);
INSERT INTO `settings` VALUES ('password_reset', '1', 0, 0);
INSERT INTO `settings` VALUES ('subtask_restriction', '0', 0, 0);
INSERT INTO `settings` VALUES ('subtask_time_tracking', '1', 0, 0);
INSERT INTO `settings` VALUES ('webhook_token', '5205cda661cb376ef3bc683345ceae8d0a0d6ab3987b46d476b14539a2cb', 0, 0);
INSERT INTO `settings` VALUES ('webhook_url', '', 0, 0);

-- ----------------------------
-- Table structure for subtask_time_tracking
-- ----------------------------
DROP TABLE IF EXISTS `subtask_time_tracking`;
CREATE TABLE `subtask_time_tracking`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `subtask_id` int(11) NOT NULL,
  `start` bigint(20) NULL DEFAULT NULL,
  `end` bigint(20) NULL DEFAULT NULL,
  `time_spent` float NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `subtask_id`(`subtask_id`) USING BTREE,
  CONSTRAINT `subtask_time_tracking_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `subtask_time_tracking_ibfk_2` FOREIGN KEY (`subtask_id`) REFERENCES `subtasks` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for subtasks
-- ----------------------------
DROP TABLE IF EXISTS `subtasks`;
CREATE TABLE `subtasks`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `status` int(11) NULL DEFAULT 0,
  `time_estimated` float NULL DEFAULT NULL,
  `time_spent` float NULL DEFAULT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) NULL DEFAULT NULL,
  `position` int(11) NULL DEFAULT 1,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `task_id`(`task_id`) USING BTREE,
  CONSTRAINT `subtasks_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for swimlanes
-- ----------------------------
DROP TABLE IF EXISTS `swimlanes`;
CREATE TABLE `swimlanes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `position` int(11) NULL DEFAULT 1,
  `is_active` int(11) NULL DEFAULT 1,
  `project_id` int(11) NULL DEFAULT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`, `project_id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  CONSTRAINT `swimlanes_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tags
-- ----------------------------
DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `name`(`name`, `project_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for task_has_external_links
-- ----------------------------
DROP TABLE IF EXISTS `task_has_external_links`;
CREATE TABLE `task_has_external_links`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link_type` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `dependency` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `date_creation` int(11) NOT NULL,
  `date_modification` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `creator_id` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `task_id`(`task_id`) USING BTREE,
  CONSTRAINT `task_has_external_links_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for task_has_files
-- ----------------------------
DROP TABLE IF EXISTS `task_has_files`;
CREATE TABLE `task_has_files`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_image` tinyint(1) NULL DEFAULT 0,
  `task_id` int(11) NOT NULL,
  `date` int(11) NOT NULL DEFAULT 0,
  `user_id` int(11) NOT NULL DEFAULT 0,
  `size` int(11) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `task_id`(`task_id`) USING BTREE,
  CONSTRAINT `task_has_files_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for task_has_links
-- ----------------------------
DROP TABLE IF EXISTS `task_has_links`;
CREATE TABLE `task_has_links`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `opposite_task_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `link_id`(`link_id`, `task_id`, `opposite_task_id`) USING BTREE,
  INDEX `task_id`(`task_id`) USING BTREE,
  INDEX `opposite_task_id`(`opposite_task_id`) USING BTREE,
  CONSTRAINT `task_has_links_ibfk_1` FOREIGN KEY (`link_id`) REFERENCES `links` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `task_has_links_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `task_has_links_ibfk_3` FOREIGN KEY (`opposite_task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for task_has_metadata
-- ----------------------------
DROP TABLE IF EXISTS `task_has_metadata`;
CREATE TABLE `task_has_metadata`  (
  `task_id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `changed_by` int(11) NOT NULL DEFAULT 0,
  `changed_on` int(11) NOT NULL DEFAULT 0,
  UNIQUE INDEX `task_id`(`task_id`, `name`) USING BTREE,
  CONSTRAINT `task_has_metadata_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for task_has_tags
-- ----------------------------
DROP TABLE IF EXISTS `task_has_tags`;
CREATE TABLE `task_has_tags`  (
  `task_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL,
  UNIQUE INDEX `task_id`(`task_id`, `tag_id`) USING BTREE,
  INDEX `tag_id`(`tag_id`) USING BTREE,
  CONSTRAINT `task_has_tags_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `task_has_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for tasks
-- ----------------------------
DROP TABLE IF EXISTS `tasks`;
CREATE TABLE `tasks`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `description` text CHARACTER SET utf8 COLLATE utf8_general_ci NULL,
  `date_creation` bigint(20) NULL DEFAULT NULL,
  `date_completed` bigint(20) NULL DEFAULT NULL,
  `date_due` bigint(20) NULL DEFAULT NULL,
  `color_id` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `owner_id` int(11) NULL DEFAULT 0,
  `position` int(11) NULL DEFAULT NULL,
  `score` int(11) NULL DEFAULT NULL,
  `is_active` tinyint(1) NULL DEFAULT 1,
  `category_id` int(11) NULL DEFAULT 0,
  `creator_id` int(11) NULL DEFAULT 0,
  `date_modification` int(11) NULL DEFAULT 0,
  `reference` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `date_started` bigint(20) NULL DEFAULT NULL,
  `time_spent` float NULL DEFAULT 0,
  `time_estimated` float NULL DEFAULT 0,
  `swimlane_id` int(11) NULL DEFAULT 0,
  `date_moved` bigint(20) NULL DEFAULT NULL,
  `recurrence_status` int(11) NOT NULL DEFAULT 0,
  `recurrence_trigger` int(11) NOT NULL DEFAULT 0,
  `recurrence_factor` int(11) NOT NULL DEFAULT 0,
  `recurrence_timeframe` int(11) NOT NULL DEFAULT 0,
  `recurrence_basedate` int(11) NOT NULL DEFAULT 0,
  `recurrence_parent` int(11) NULL DEFAULT NULL,
  `recurrence_child` int(11) NULL DEFAULT NULL,
  `priority` int(11) NULL DEFAULT 0,
  `progress` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `is_active`(`is_active`) USING BTREE,
  INDEX `reference`(`reference`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  INDEX `column_id`(`column_id`) USING BTREE,
  CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for transitions
-- ----------------------------
DROP TABLE IF EXISTS `transitions`;
CREATE TABLE `transitions`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `src_column_id` int(11) NOT NULL,
  `dst_column_id` int(11) NOT NULL,
  `date` bigint(20) NULL DEFAULT NULL,
  `time_spent` int(11) NULL DEFAULT 0,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `src_column_id`(`src_column_id`) USING BTREE,
  INDEX `dst_column_id`(`dst_column_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  INDEX `task_id`(`task_id`) USING BTREE,
  CONSTRAINT `transitions_ibfk_1` FOREIGN KEY (`src_column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `transitions_ibfk_2` FOREIGN KEY (`dst_column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `transitions_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `transitions_ibfk_4` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `transitions_ibfk_5` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_has_metadata
-- ----------------------------
DROP TABLE IF EXISTS `user_has_metadata`;
CREATE TABLE `user_has_metadata`  (
  `user_id` int(11) NOT NULL,
  `name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `value` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `changed_by` int(11) NOT NULL DEFAULT 0,
  `changed_on` int(11) NOT NULL DEFAULT 0,
  UNIQUE INDEX `user_id`(`user_id`, `name`) USING BTREE,
  CONSTRAINT `user_has_metadata_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_has_notification_types
-- ----------------------------
DROP TABLE IF EXISTS `user_has_notification_types`;
CREATE TABLE `user_has_notification_types`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `notification_type` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_id`(`user_id`, `notification_type`) USING BTREE,
  CONSTRAINT `user_has_notification_types_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_has_notifications
-- ----------------------------
DROP TABLE IF EXISTS `user_has_notifications`;
CREATE TABLE `user_has_notifications`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `user_id`(`user_id`, `project_id`) USING BTREE,
  INDEX `project_id`(`project_id`) USING BTREE,
  CONSTRAINT `user_has_notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT,
  CONSTRAINT `user_has_notifications_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for user_has_unread_notifications
-- ----------------------------
DROP TABLE IF EXISTS `user_has_unread_notifications`;
CREATE TABLE `user_has_unread_notifications`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `date_creation` bigint(20) NOT NULL,
  `event_name` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `event_data` text CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `user_has_unread_notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE RESTRICT
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `is_ldap_user` tinyint(1) NULL DEFAULT 0,
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `google_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `github_id` varchar(30) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notifications_enabled` tinyint(1) NULL DEFAULT 0,
  `timezone` varchar(50) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `language` varchar(5) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `disable_login_form` tinyint(1) NULL DEFAULT 0,
  `twofactor_activated` tinyint(1) NULL DEFAULT 0,
  `twofactor_secret` varchar(16) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '',
  `api_token` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `notifications_filter` int(11) NULL DEFAULT 4,
  `nb_failed_login` int(11) NULL DEFAULT 0,
  `lock_expiration_date` bigint(20) NULL DEFAULT NULL,
  `gitlab_id` int(11) NULL DEFAULT NULL,
  `role` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL DEFAULT 'app-user',
  `is_active` tinyint(1) NULL DEFAULT 1,
  `avatar_path` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `skin` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `layout` varchar(15) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  `dashboard` varchar(25) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE,
  UNIQUE INDEX `email`(`email`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES (1, 'admin', '$2y$10$xJ8gcSzxzLgTcEknwdOvE.w/.btkNO6R6vNTgxi58kfbz6sE3a0su', 0, NULL, 'admin@admin.com', NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '', NULL, 4, 0, 0, NULL, 'app-admin', 1, NULL, NULL, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
