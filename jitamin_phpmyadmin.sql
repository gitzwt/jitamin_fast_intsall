-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 2018-09-28 04:16:02
-- 服务器版本： 5.7.20
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `jitamin`
--

-- --------------------------------------------------------

--
-- 表的结构 `actions`
--

CREATE TABLE `actions` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `event_name` varchar(50) NOT NULL,
  `action_name` varchar(255) DEFAULT NULL,
  `position` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `action_has_params`
--

CREATE TABLE `action_has_params` (
  `id` int(11) NOT NULL,
  `action_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `columns`
--

CREATE TABLE `columns` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `position` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `task_limit` int(11) DEFAULT '0',
  `description` text,
  `hide_in_dashboard` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `column_has_move_restrictions`
--

CREATE TABLE `column_has_move_restrictions` (
  `restriction_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `src_column_id` int(11) NOT NULL,
  `dst_column_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `column_has_restrictions`
--

CREATE TABLE `column_has_restrictions` (
  `restriction_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `rule` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT '0',
  `date_creation` bigint(20) DEFAULT NULL,
  `comment` text,
  `reference` varchar(50) DEFAULT ''
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `custom_filters`
--

CREATE TABLE `custom_filters` (
  `id` int(11) NOT NULL,
  `filter` varchar(100) NOT NULL,
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `is_shared` tinyint(1) DEFAULT '0',
  `append` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `external_id` varchar(255) DEFAULT '',
  `name` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `group_has_users`
--

CREATE TABLE `group_has_users` (
  `group_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `last_logins`
--

CREATE TABLE `last_logins` (
  `id` int(11) NOT NULL,
  `auth_type` varchar(25) DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `date_creation` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `last_logins`
--

INSERT INTO `last_logins` (`id`, `auth_type`, `user_id`, `ip`, `user_agent`, `date_creation`) VALUES
(1, 'Database', 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', 1538107837);

-- --------------------------------------------------------

--
-- 表的结构 `links`
--

CREATE TABLE `links` (
  `id` int(11) NOT NULL,
  `label` varchar(255) NOT NULL,
  `opposite_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `links`
--

INSERT INTO `links` (`id`, `label`, `opposite_id`) VALUES
(1, 'relates to', 0),
(2, 'blocks', 3),
(3, 'is blocked by', 2),
(4, 'duplicates', 5),
(5, 'is duplicated by', 4),
(6, 'is a child of', 7),
(7, 'is a parent of', 6),
(8, 'targets milestone', 9),
(9, 'is a milestone of', 8),
(10, 'fixes', 11),
(11, 'is fixed by', 10);

-- --------------------------------------------------------

--
-- 表的结构 `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL,
  `migration_name` varchar(100) DEFAULT NULL,
  `start_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `end_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `breakpoint` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `migrations`
--

INSERT INTO `migrations` (`version`, `migration_name`, `start_time`, `end_time`, `breakpoint`) VALUES
(20161222061456, 'CreateUsersTable', '2018-09-27 20:09:34', '2018-09-27 20:09:34', 0),
(20161222065743, 'CreateRememberMeTable', '2018-09-27 20:09:34', '2018-09-27 20:09:34', 0),
(20161222071058, 'CreateGroupsTable', '2018-09-27 20:09:34', '2018-09-27 20:09:34', 0),
(20161222071513, 'CreateSettingsTable', '2018-09-27 20:09:34', '2018-09-27 20:09:34', 0),
(20161222072332, 'CreateProjectsTable', '2018-09-27 20:09:34', '2018-09-27 20:09:34', 0),
(20161222073541, 'CreateActionsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222073852, 'CreateColumnsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222074452, 'CreateTasksTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222081719, 'CreateCommentsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222082417, 'CreateSwimlanesTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222083010, 'CreateTagsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222083245, 'CreateSubtasksTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222083935, 'CreateLinksTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222084249, 'CreateTransitionsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222084940, 'CreateCustomFiltersTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222085354, 'CreateLastLoginsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222085809, 'CreatePasswordResetTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222091052, 'CreatePluginSchemaVersionsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222091605, 'CreateProjectActivitiesTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222092217, 'CreateProjectDailyColumnStatsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222092312, 'CreateProjectDailyStatsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222093033, 'CreateSchemaVersionTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222093333, 'CreateActionHasParamsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222094356, 'CreateProjectHasRolesTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222094851, 'CreateColumnHasRestrictionsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222094859, 'CreateColumnHasMoveRestrictionsTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222095207, 'CreateGroupHasUsersTable', '2018-09-27 20:09:35', '2018-09-27 20:09:35', 0),
(20161222095739, 'CreateProjectHasCategoriesTable', '2018-09-27 20:09:35', '2018-09-27 20:09:36', 0),
(20161222100221, 'CreateProjectHasFilesTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222104316, 'CreateProjectHasGroupsTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222104338, 'CreateProjectHasMetadataTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222104355, 'CreateProjectHasStarsTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222104411, 'CreateProjectHasNotificationTypesTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222104427, 'CreateProjectHasUsersTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222112306, 'CreateProjectRoleHasRestrictionsTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222112615, 'CreateSubtaskTimeTrackingTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222113157, 'CreateTaskHasExternalLinksTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222113205, 'CreateTaskHasFilesTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222113217, 'CreateTaskHasLinksTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222113234, 'CreateTaskHasMetadataTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222113239, 'CreateTaskHasTagsTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222114814, 'CreateUserHasMetadataTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222114828, 'CreateUserHasNotificationTypesTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222114837, 'CreateUserHasNotificationsTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161222114844, 'CreateUserHasUnreadNotificationsTable', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161225123941, 'AlterTableUsersAddApiTokenColumn', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161228031419, 'AlterTableUsersAddLayoutColumn', '2018-09-27 20:09:36', '2018-09-27 20:09:36', 0),
(20161231134810, 'AlterTableUsersAddDashboardColumn', '2018-09-27 20:09:36', '2018-09-27 20:09:37', 0),
(20170105040003, 'AlterTableProjectsAddDefaultViewColumn', '2018-09-27 20:09:37', '2018-09-27 20:09:37', 0),
(20171228053201, 'AlterTableActionsAddPositionColumn', '2018-09-27 20:09:37', '2018-09-27 20:09:37', 0);

-- --------------------------------------------------------

--
-- 表的结构 `password_reset`
--

CREATE TABLE `password_reset` (
  `id` int(11) NOT NULL,
  `token` varchar(80) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_expiration` int(11) NOT NULL,
  `date_creation` int(11) NOT NULL,
  `ip` varchar(45) NOT NULL,
  `user_agent` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `plugin_schema_versions`
--

CREATE TABLE `plugin_schema_versions` (
  `plugin` varchar(80) NOT NULL,
  `version` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `token` varchar(255) DEFAULT NULL,
  `last_modified` bigint(20) DEFAULT NULL,
  `is_public` tinyint(1) DEFAULT '0',
  `is_private` tinyint(1) DEFAULT '0',
  `is_everybody_allowed` tinyint(1) DEFAULT '0',
  `default_swimlane` varchar(200) DEFAULT 'Default swimlane',
  `default_view` varchar(25) DEFAULT NULL,
  `show_default_swimlane` int(11) DEFAULT '1',
  `description` text,
  `identifier` varchar(50) DEFAULT '',
  `start_date` varchar(10) DEFAULT '',
  `end_date` varchar(10) DEFAULT '',
  `owner_id` int(11) DEFAULT '0',
  `priority_default` int(11) DEFAULT '0',
  `priority_start` int(11) DEFAULT '0',
  `priority_end` int(11) DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_activities`
--

CREATE TABLE `project_activities` (
  `id` int(11) NOT NULL,
  `date_creation` bigint(20) DEFAULT NULL,
  `event_name` varchar(50) NOT NULL,
  `creator_id` int(11) DEFAULT NULL,
  `project_id` int(11) DEFAULT NULL,
  `task_id` int(11) DEFAULT NULL,
  `data` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_daily_column_stats`
--

CREATE TABLE `project_daily_column_stats` (
  `id` int(11) NOT NULL,
  `day` varchar(10) NOT NULL,
  `project_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `total` int(11) NOT NULL DEFAULT '0',
  `score` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_daily_stats`
--

CREATE TABLE `project_daily_stats` (
  `id` int(11) NOT NULL,
  `day` varchar(10) NOT NULL,
  `project_id` int(11) NOT NULL,
  `avg_lead_time` int(11) NOT NULL DEFAULT '0',
  `avg_cycle_time` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_has_categories`
--

CREATE TABLE `project_has_categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `project_id` int(11) NOT NULL,
  `description` text,
  `position` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_has_files`
--

CREATE TABLE `project_has_files` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `is_image` tinyint(1) DEFAULT '0',
  `size` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `date` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_has_groups`
--

CREATE TABLE `project_has_groups` (
  `group_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_has_metadata`
--

CREATE TABLE `project_has_metadata` (
  `project_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(255) DEFAULT '',
  `changed_by` int(11) NOT NULL DEFAULT '0',
  `changed_on` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_has_notification_types`
--

CREATE TABLE `project_has_notification_types` (
  `id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `notification_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_has_roles`
--

CREATE TABLE `project_has_roles` (
  `role_id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_has_stars`
--

CREATE TABLE `project_has_stars` (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_has_users`
--

CREATE TABLE `project_has_users` (
  `project_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `role` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `project_role_has_restrictions`
--

CREATE TABLE `project_role_has_restrictions` (
  `restriction_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `rule` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `remember_me`
--

CREATE TABLE `remember_me` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip` varchar(45) DEFAULT NULL,
  `user_agent` varchar(255) DEFAULT NULL,
  `token` varchar(255) DEFAULT NULL,
  `sequence` varchar(255) DEFAULT NULL,
  `expiration` int(11) DEFAULT NULL,
  `date_creation` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `remember_me`
--

INSERT INTO `remember_me` (`id`, `user_id`, `ip`, `user_agent`, `token`, `sequence`, `expiration`, `date_creation`) VALUES
(1, 1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36', '31c9421840a66d736da015184afd509875ea97630eb55ed14b9db2dd85859049', '49a8442ae27c6ced43b329ae6a36d0ede4753fcf360d6fa21d1cef4e414d', 1540699837, 1538107837);

-- --------------------------------------------------------

--
-- 表的结构 `schema_version`
--

CREATE TABLE `schema_version` (
  `version` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `settings`
--

CREATE TABLE `settings` (
  `option` varchar(100) NOT NULL,
  `value` varchar(255) DEFAULT '',
  `changed_by` int(11) NOT NULL DEFAULT '0',
  `changed_on` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `settings`
--

INSERT INTO `settings` (`option`, `value`, `changed_by`, `changed_on`) VALUES
('api_token', 'd8ea8087f454daa78d4dde7758f83f99cf7196591e28e927f6eacecdafbd', 0, 0),
('application_datetime_format', 'm/d/Y H:i', 1, 1538107872),
('application_date_format', 'm/d/Y', 1, 1538107872),
('application_language', 'zh_CN', 1, 1538107872),
('application_skin', 'default', 0, 0),
('application_timezone', 'UTC', 1, 1538107872),
('application_time_format', 'H:i', 1, 1538107872),
('board_columns', '', 0, 0),
('board_highlight_period', '172800', 0, 0),
('board_private_refresh_interval', '10', 0, 0),
('board_public_refresh_interval', '60', 0, 0),
('calendar_project_tasks', 'date_started', 0, 0),
('calendar_user_subtasks_time_tracking', '0', 0, 0),
('calendar_user_tasks', 'date_started', 0, 0),
('cfd_include_closed_tasks', '1', 0, 0),
('default_color', 'yellow', 0, 0),
('integration_gravatar', '0', 0, 0),
('password_reset', '1', 0, 0),
('subtask_restriction', '0', 0, 0),
('subtask_time_tracking', '1', 0, 0),
('webhook_token', '5205cda661cb376ef3bc683345ceae8d0a0d6ab3987b46d476b14539a2cb', 0, 0),
('webhook_url', '', 0, 0);

-- --------------------------------------------------------

--
-- 表的结构 `subtasks`
--

CREATE TABLE `subtasks` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `status` int(11) DEFAULT '0',
  `time_estimated` float DEFAULT NULL,
  `time_spent` float DEFAULT NULL,
  `task_id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `position` int(11) DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `subtask_time_tracking`
--

CREATE TABLE `subtask_time_tracking` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subtask_id` int(11) NOT NULL,
  `start` bigint(20) DEFAULT NULL,
  `end` bigint(20) DEFAULT NULL,
  `time_spent` float DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `swimlanes`
--

CREATE TABLE `swimlanes` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `position` int(11) DEFAULT '1',
  `is_active` int(11) DEFAULT '1',
  `project_id` int(11) DEFAULT NULL,
  `description` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `tasks`
--

CREATE TABLE `tasks` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text,
  `date_creation` bigint(20) DEFAULT NULL,
  `date_completed` bigint(20) DEFAULT NULL,
  `date_due` bigint(20) DEFAULT NULL,
  `color_id` varchar(50) DEFAULT NULL,
  `project_id` int(11) NOT NULL,
  `column_id` int(11) NOT NULL,
  `owner_id` int(11) DEFAULT '0',
  `position` int(11) DEFAULT NULL,
  `score` int(11) DEFAULT NULL,
  `is_active` tinyint(1) DEFAULT '1',
  `category_id` int(11) DEFAULT '0',
  `creator_id` int(11) DEFAULT '0',
  `date_modification` int(11) DEFAULT '0',
  `reference` varchar(50) DEFAULT '',
  `date_started` bigint(20) DEFAULT NULL,
  `time_spent` float DEFAULT '0',
  `time_estimated` float DEFAULT '0',
  `swimlane_id` int(11) DEFAULT '0',
  `date_moved` bigint(20) DEFAULT NULL,
  `recurrence_status` int(11) NOT NULL DEFAULT '0',
  `recurrence_trigger` int(11) NOT NULL DEFAULT '0',
  `recurrence_factor` int(11) NOT NULL DEFAULT '0',
  `recurrence_timeframe` int(11) NOT NULL DEFAULT '0',
  `recurrence_basedate` int(11) NOT NULL DEFAULT '0',
  `recurrence_parent` int(11) DEFAULT NULL,
  `recurrence_child` int(11) DEFAULT NULL,
  `priority` int(11) DEFAULT '0',
  `progress` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `task_has_external_links`
--

CREATE TABLE `task_has_external_links` (
  `id` int(11) NOT NULL,
  `link_type` varchar(100) NOT NULL,
  `dependency` varchar(100) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `date_creation` int(11) NOT NULL,
  `date_modification` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `creator_id` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `task_has_files`
--

CREATE TABLE `task_has_files` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `path` varchar(255) NOT NULL,
  `is_image` tinyint(1) DEFAULT '0',
  `task_id` int(11) NOT NULL,
  `date` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `size` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `task_has_links`
--

CREATE TABLE `task_has_links` (
  `id` int(11) NOT NULL,
  `link_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `opposite_task_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `task_has_metadata`
--

CREATE TABLE `task_has_metadata` (
  `task_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(255) DEFAULT '',
  `changed_by` int(11) NOT NULL DEFAULT '0',
  `changed_on` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `task_has_tags`
--

CREATE TABLE `task_has_tags` (
  `task_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `transitions`
--

CREATE TABLE `transitions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `task_id` int(11) NOT NULL,
  `src_column_id` int(11) NOT NULL,
  `dst_column_id` int(11) NOT NULL,
  `date` bigint(20) DEFAULT NULL,
  `time_spent` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `is_ldap_user` tinyint(1) DEFAULT '0',
  `name` varchar(255) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `google_id` varchar(30) DEFAULT NULL,
  `github_id` varchar(30) DEFAULT NULL,
  `notifications_enabled` tinyint(1) DEFAULT '0',
  `timezone` varchar(50) DEFAULT NULL,
  `language` varchar(5) DEFAULT NULL,
  `disable_login_form` tinyint(1) DEFAULT '0',
  `twofactor_activated` tinyint(1) DEFAULT '0',
  `twofactor_secret` varchar(16) DEFAULT NULL,
  `token` varchar(255) DEFAULT '',
  `api_token` varchar(255) DEFAULT NULL,
  `notifications_filter` int(11) DEFAULT '4',
  `nb_failed_login` int(11) DEFAULT '0',
  `lock_expiration_date` bigint(20) DEFAULT NULL,
  `gitlab_id` int(11) DEFAULT NULL,
  `role` varchar(25) NOT NULL DEFAULT 'app-user',
  `is_active` tinyint(1) DEFAULT '1',
  `avatar_path` varchar(255) DEFAULT NULL,
  `skin` varchar(15) DEFAULT NULL,
  `layout` varchar(15) DEFAULT NULL,
  `dashboard` varchar(25) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- 转存表中的数据 `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `is_ldap_user`, `name`, `email`, `google_id`, `github_id`, `notifications_enabled`, `timezone`, `language`, `disable_login_form`, `twofactor_activated`, `twofactor_secret`, `token`, `api_token`, `notifications_filter`, `nb_failed_login`, `lock_expiration_date`, `gitlab_id`, `role`, `is_active`, `avatar_path`, `skin`, `layout`, `dashboard`) VALUES
(1, 'admin', '$2y$10$xJ8gcSzxzLgTcEknwdOvE.w/.btkNO6R6vNTgxi58kfbz6sE3a0su', 0, NULL, 'admin@admin.com', NULL, NULL, 0, NULL, NULL, 0, 0, NULL, '', NULL, 4, 0, 0, NULL, 'app-admin', 1, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- 表的结构 `user_has_metadata`
--

CREATE TABLE `user_has_metadata` (
  `user_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `value` varchar(255) DEFAULT '',
  `changed_by` int(11) NOT NULL DEFAULT '0',
  `changed_on` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_has_notifications`
--

CREATE TABLE `user_has_notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_has_notification_types`
--

CREATE TABLE `user_has_notification_types` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `notification_type` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- 表的结构 `user_has_unread_notifications`
--

CREATE TABLE `user_has_unread_notifications` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_creation` bigint(20) NOT NULL,
  `event_name` varchar(50) NOT NULL,
  `event_data` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `action_has_params`
--
ALTER TABLE `action_has_params`
  ADD PRIMARY KEY (`id`),
  ADD KEY `action_id` (`action_id`);

--
-- Indexes for table `columns`
--
ALTER TABLE `columns`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`,`project_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `column_has_move_restrictions`
--
ALTER TABLE `column_has_move_restrictions`
  ADD PRIMARY KEY (`restriction_id`),
  ADD UNIQUE KEY `role_id` (`role_id`,`src_column_id`,`dst_column_id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `src_column_id` (`src_column_id`),
  ADD KEY `dst_column_id` (`dst_column_id`);

--
-- Indexes for table `column_has_restrictions`
--
ALTER TABLE `column_has_restrictions`
  ADD PRIMARY KEY (`restriction_id`),
  ADD UNIQUE KEY `role_id` (`role_id`,`column_id`,`rule`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `column_id` (`column_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `reference` (`reference`),
  ADD KEY `task_id` (`task_id`);

--
-- Indexes for table `custom_filters`
--
ALTER TABLE `custom_filters`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `group_has_users`
--
ALTER TABLE `group_has_users`
  ADD UNIQUE KEY `group_id` (`group_id`,`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `last_logins`
--
ALTER TABLE `last_logins`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `label` (`label`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`version`);

--
-- Indexes for table `password_reset`
--
ALTER TABLE `password_reset`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `plugin_schema_versions`
--
ALTER TABLE `plugin_schema_versions`
  ADD PRIMARY KEY (`plugin`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_activities`
--
ALTER TABLE `project_activities`
  ADD PRIMARY KEY (`id`),
  ADD KEY `creator_id` (`creator_id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `task_id` (`task_id`);

--
-- Indexes for table `project_daily_column_stats`
--
ALTER TABLE `project_daily_column_stats`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `day` (`day`,`project_id`,`column_id`),
  ADD KEY `column_id` (`column_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `project_daily_stats`
--
ALTER TABLE `project_daily_stats`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `day` (`day`,`project_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `project_has_categories`
--
ALTER TABLE `project_has_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `project_id` (`project_id`,`name`);

--
-- Indexes for table `project_has_files`
--
ALTER TABLE `project_has_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `project_has_groups`
--
ALTER TABLE `project_has_groups`
  ADD UNIQUE KEY `group_id` (`group_id`,`project_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `project_has_metadata`
--
ALTER TABLE `project_has_metadata`
  ADD UNIQUE KEY `project_id` (`project_id`,`name`);

--
-- Indexes for table `project_has_notification_types`
--
ALTER TABLE `project_has_notification_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `project_id` (`project_id`,`notification_type`);

--
-- Indexes for table `project_has_roles`
--
ALTER TABLE `project_has_roles`
  ADD PRIMARY KEY (`role_id`),
  ADD UNIQUE KEY `project_id` (`project_id`,`role`);

--
-- Indexes for table `project_has_stars`
--
ALTER TABLE `project_has_stars`
  ADD UNIQUE KEY `project_id` (`project_id`,`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `project_has_users`
--
ALTER TABLE `project_has_users`
  ADD UNIQUE KEY `project_id` (`project_id`,`user_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `project_role_has_restrictions`
--
ALTER TABLE `project_role_has_restrictions`
  ADD PRIMARY KEY (`restriction_id`),
  ADD UNIQUE KEY `role_id` (`role_id`,`rule`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `remember_me`
--
ALTER TABLE `remember_me`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`option`);

--
-- Indexes for table `subtasks`
--
ALTER TABLE `subtasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_id` (`task_id`);

--
-- Indexes for table `subtask_time_tracking`
--
ALTER TABLE `subtask_time_tracking`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `subtask_id` (`subtask_id`);

--
-- Indexes for table `swimlanes`
--
ALTER TABLE `swimlanes`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`,`project_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`,`project_id`);

--
-- Indexes for table `tasks`
--
ALTER TABLE `tasks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `is_active` (`is_active`),
  ADD KEY `reference` (`reference`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `column_id` (`column_id`);

--
-- Indexes for table `task_has_external_links`
--
ALTER TABLE `task_has_external_links`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_id` (`task_id`);

--
-- Indexes for table `task_has_files`
--
ALTER TABLE `task_has_files`
  ADD PRIMARY KEY (`id`),
  ADD KEY `task_id` (`task_id`);

--
-- Indexes for table `task_has_links`
--
ALTER TABLE `task_has_links`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `link_id` (`link_id`,`task_id`,`opposite_task_id`),
  ADD KEY `task_id` (`task_id`),
  ADD KEY `opposite_task_id` (`opposite_task_id`);

--
-- Indexes for table `task_has_metadata`
--
ALTER TABLE `task_has_metadata`
  ADD UNIQUE KEY `task_id` (`task_id`,`name`);

--
-- Indexes for table `task_has_tags`
--
ALTER TABLE `task_has_tags`
  ADD UNIQUE KEY `task_id` (`task_id`,`tag_id`),
  ADD KEY `tag_id` (`tag_id`);

--
-- Indexes for table `transitions`
--
ALTER TABLE `transitions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `src_column_id` (`src_column_id`),
  ADD KEY `dst_column_id` (`dst_column_id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `project_id` (`project_id`),
  ADD KEY `task_id` (`task_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `user_has_metadata`
--
ALTER TABLE `user_has_metadata`
  ADD UNIQUE KEY `user_id` (`user_id`,`name`);

--
-- Indexes for table `user_has_notifications`
--
ALTER TABLE `user_has_notifications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`project_id`),
  ADD KEY `project_id` (`project_id`);

--
-- Indexes for table `user_has_notification_types`
--
ALTER TABLE `user_has_notification_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`,`notification_type`);

--
-- Indexes for table `user_has_unread_notifications`
--
ALTER TABLE `user_has_unread_notifications`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- 在导出的表使用AUTO_INCREMENT
--

--
-- 使用表AUTO_INCREMENT `actions`
--
ALTER TABLE `actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `action_has_params`
--
ALTER TABLE `action_has_params`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `columns`
--
ALTER TABLE `columns`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `column_has_move_restrictions`
--
ALTER TABLE `column_has_move_restrictions`
  MODIFY `restriction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `column_has_restrictions`
--
ALTER TABLE `column_has_restrictions`
  MODIFY `restriction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `custom_filters`
--
ALTER TABLE `custom_filters`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `last_logins`
--
ALTER TABLE `last_logins`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `links`
--
ALTER TABLE `links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- 使用表AUTO_INCREMENT `password_reset`
--
ALTER TABLE `password_reset`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `project_activities`
--
ALTER TABLE `project_activities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `project_daily_column_stats`
--
ALTER TABLE `project_daily_column_stats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `project_daily_stats`
--
ALTER TABLE `project_daily_stats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `project_has_categories`
--
ALTER TABLE `project_has_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `project_has_files`
--
ALTER TABLE `project_has_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `project_has_notification_types`
--
ALTER TABLE `project_has_notification_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `project_has_roles`
--
ALTER TABLE `project_has_roles`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `project_role_has_restrictions`
--
ALTER TABLE `project_role_has_restrictions`
  MODIFY `restriction_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `remember_me`
--
ALTER TABLE `remember_me`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `subtasks`
--
ALTER TABLE `subtasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `subtask_time_tracking`
--
ALTER TABLE `subtask_time_tracking`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `swimlanes`
--
ALTER TABLE `swimlanes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `tasks`
--
ALTER TABLE `tasks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `task_has_external_links`
--
ALTER TABLE `task_has_external_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `task_has_files`
--
ALTER TABLE `task_has_files`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `task_has_links`
--
ALTER TABLE `task_has_links`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `transitions`
--
ALTER TABLE `transitions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- 使用表AUTO_INCREMENT `user_has_notifications`
--
ALTER TABLE `user_has_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `user_has_notification_types`
--
ALTER TABLE `user_has_notification_types`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 使用表AUTO_INCREMENT `user_has_unread_notifications`
--
ALTER TABLE `user_has_unread_notifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- 限制导出的表
--

--
-- 限制表 `actions`
--
ALTER TABLE `actions`
  ADD CONSTRAINT `actions_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `action_has_params`
--
ALTER TABLE `action_has_params`
  ADD CONSTRAINT `action_has_params_ibfk_1` FOREIGN KEY (`action_id`) REFERENCES `actions` (`id`) ON DELETE CASCADE;

--
-- 限制表 `columns`
--
ALTER TABLE `columns`
  ADD CONSTRAINT `columns_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `column_has_move_restrictions`
--
ALTER TABLE `column_has_move_restrictions`
  ADD CONSTRAINT `column_has_move_restrictions_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `column_has_move_restrictions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `project_has_roles` (`role_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `column_has_move_restrictions_ibfk_3` FOREIGN KEY (`src_column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `column_has_move_restrictions_ibfk_4` FOREIGN KEY (`dst_column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE;

--
-- 限制表 `column_has_restrictions`
--
ALTER TABLE `column_has_restrictions`
  ADD CONSTRAINT `column_has_restrictions_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `column_has_restrictions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `project_has_roles` (`role_id`) ON DELETE CASCADE,
  ADD CONSTRAINT `column_has_restrictions_ibfk_3` FOREIGN KEY (`column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE;

--
-- 限制表 `comments`
--
ALTER TABLE `comments`
  ADD CONSTRAINT `comments_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE;

--
-- 限制表 `custom_filters`
--
ALTER TABLE `custom_filters`
  ADD CONSTRAINT `custom_filters_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `custom_filters_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `group_has_users`
--
ALTER TABLE `group_has_users`
  ADD CONSTRAINT `group_has_users_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `group_has_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- 限制表 `last_logins`
--
ALTER TABLE `last_logins`
  ADD CONSTRAINT `last_logins_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- 限制表 `password_reset`
--
ALTER TABLE `password_reset`
  ADD CONSTRAINT `password_reset_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_activities`
--
ALTER TABLE `project_activities`
  ADD CONSTRAINT `project_activities_ibfk_1` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_activities_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_activities_ibfk_3` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_daily_column_stats`
--
ALTER TABLE `project_daily_column_stats`
  ADD CONSTRAINT `project_daily_column_stats_ibfk_1` FOREIGN KEY (`column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_daily_column_stats_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_daily_stats`
--
ALTER TABLE `project_daily_stats`
  ADD CONSTRAINT `project_daily_stats_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_has_categories`
--
ALTER TABLE `project_has_categories`
  ADD CONSTRAINT `project_has_categories_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_has_files`
--
ALTER TABLE `project_has_files`
  ADD CONSTRAINT `project_has_files_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_has_groups`
--
ALTER TABLE `project_has_groups`
  ADD CONSTRAINT `project_has_groups_ibfk_1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_has_groups_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_has_metadata`
--
ALTER TABLE `project_has_metadata`
  ADD CONSTRAINT `project_has_metadata_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_has_notification_types`
--
ALTER TABLE `project_has_notification_types`
  ADD CONSTRAINT `project_has_notification_types_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_has_roles`
--
ALTER TABLE `project_has_roles`
  ADD CONSTRAINT `project_has_roles_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_has_stars`
--
ALTER TABLE `project_has_stars`
  ADD CONSTRAINT `project_has_stars_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_has_stars_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_has_users`
--
ALTER TABLE `project_has_users`
  ADD CONSTRAINT `project_has_users_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_has_users_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- 限制表 `project_role_has_restrictions`
--
ALTER TABLE `project_role_has_restrictions`
  ADD CONSTRAINT `project_role_has_restrictions_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `project_role_has_restrictions_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `project_has_roles` (`role_id`) ON DELETE CASCADE;

--
-- 限制表 `remember_me`
--
ALTER TABLE `remember_me`
  ADD CONSTRAINT `remember_me_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- 限制表 `subtasks`
--
ALTER TABLE `subtasks`
  ADD CONSTRAINT `subtasks_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE;

--
-- 限制表 `subtask_time_tracking`
--
ALTER TABLE `subtask_time_tracking`
  ADD CONSTRAINT `subtask_time_tracking_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subtask_time_tracking_ibfk_2` FOREIGN KEY (`subtask_id`) REFERENCES `subtasks` (`id`) ON DELETE CASCADE;

--
-- 限制表 `swimlanes`
--
ALTER TABLE `swimlanes`
  ADD CONSTRAINT `swimlanes_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `tasks`
--
ALTER TABLE `tasks`
  ADD CONSTRAINT `tasks_ibfk_1` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tasks_ibfk_2` FOREIGN KEY (`column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE;

--
-- 限制表 `task_has_external_links`
--
ALTER TABLE `task_has_external_links`
  ADD CONSTRAINT `task_has_external_links_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE;

--
-- 限制表 `task_has_files`
--
ALTER TABLE `task_has_files`
  ADD CONSTRAINT `task_has_files_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE;

--
-- 限制表 `task_has_links`
--
ALTER TABLE `task_has_links`
  ADD CONSTRAINT `task_has_links_ibfk_1` FOREIGN KEY (`link_id`) REFERENCES `links` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `task_has_links_ibfk_2` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `task_has_links_ibfk_3` FOREIGN KEY (`opposite_task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE;

--
-- 限制表 `task_has_metadata`
--
ALTER TABLE `task_has_metadata`
  ADD CONSTRAINT `task_has_metadata_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE;

--
-- 限制表 `task_has_tags`
--
ALTER TABLE `task_has_tags`
  ADD CONSTRAINT `task_has_tags_ibfk_1` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `task_has_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- 限制表 `transitions`
--
ALTER TABLE `transitions`
  ADD CONSTRAINT `transitions_ibfk_1` FOREIGN KEY (`src_column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transitions_ibfk_2` FOREIGN KEY (`dst_column_id`) REFERENCES `columns` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transitions_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transitions_ibfk_4` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `transitions_ibfk_5` FOREIGN KEY (`task_id`) REFERENCES `tasks` (`id`) ON DELETE CASCADE;

--
-- 限制表 `user_has_metadata`
--
ALTER TABLE `user_has_metadata`
  ADD CONSTRAINT `user_has_metadata_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- 限制表 `user_has_notifications`
--
ALTER TABLE `user_has_notifications`
  ADD CONSTRAINT `user_has_notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_has_notifications_ibfk_2` FOREIGN KEY (`project_id`) REFERENCES `projects` (`id`) ON DELETE CASCADE;

--
-- 限制表 `user_has_notification_types`
--
ALTER TABLE `user_has_notification_types`
  ADD CONSTRAINT `user_has_notification_types_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- 限制表 `user_has_unread_notifications`
--
ALTER TABLE `user_has_unread_notifications`
  ADD CONSTRAINT `user_has_unread_notifications_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
